<link rel="stylesheet" type="text/css" media="screen" href="/jardin_co/public/css/style.css" />

<footer>
            <div class="footer-nav">
                <div>
                <ul >
                <li >
                        <form action='#' class='news-form'>
                            <label for="" id='news-label'>S'inscrire à la newsletter</label>
                            <input type='text' placeholder='Veuillez entrer votre adresse mail' name='news' class='news-txt'>
                            <button type='submit'><i class='fa fa-envelope'></i>Valider</button>
                        </form>
                    </li>
                </div>      
                    <li class='nav-item_copyright'>Copyright © 2020 - 2021 Jardin&co SAS</li>
                </ul>
                <ul class='footer-nav-line-height'>
                    <li ><a href='contact.html'>Nous contacter</a></li>
                    <li ><a href='mentionslegales.html'>Mentions légales</a></li>
                    <li ><a href='cgv.html'>CGV</a></li>
                </ul>
                <ul class='footer-nav-line-height'>
                    <li ><a href='faq.html'>FAQ</a></li>
                    <li ><a href='assistance.html'>A propos de Jardin&co</a></li>
                    <li ><a href='sav.html'>SAV</a></li>
                </ul>
                <ul>
                    

                </ul>
            
            </div>
        </footer>
    </body>
</html>
