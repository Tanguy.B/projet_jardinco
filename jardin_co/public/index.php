<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" media="screen" href="/jardin_co/public/css/style.css" />
    <title>Jardin&Co</title>
</head>
<header>
<?php
 require_once('header.php'); 
?> 
</header>

<body>
    <img src="/jardin_co/public/img/background_index.png"/>
    <div class ="img_produits" ><div><img src="/jardin_co/public/img/exterieure_vignette.png"/></div><div><a href="" >Plantes exterieures</a></div></div>
    <div class ="img_produits" ><div><img src="/jardin_co/public/img/amenagement_vignette.png"/></div><div><a href="" >Amenagement de jardin</a></div></div>
    <div class ="img_both"></div>
    <div class ="img_produits" ><div><img src="/jardin_co/public/img/outil_vignette.png"/></div><div><a href="" >Outil de jardinage</a></div></div>
    <div class ="img_produits" ><div><img src="/jardin_co/public/img/produit_vignette.png"/></div><div><a href="" >Produits de jardinage</a></div></div>
    <div class ="img_both"></div>
</body>

<footer>
<?php require_once('footer.php'); 
?> 
</footer>
</html>